# bzflag
# Copyright 1993-1999, Chris Schoeneman
#
# This package is free software;  you can redistribute it and/or
# modify it under the terms of the license found in the file
# named LICENSE that should have accompanied this file.
#
# THIS PACKAGE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
# WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

#
# configuration for irix mips3
#
CONFIG  = irix-mips3

#
# tools
#
AR      = $(TOOLROOT)/usr/bin/ar cru
CC	= $(TOOLROOT)/usr/bin/cc
CXX	= $(TOOLROOT)/usr/bin/CC
CD	= cd
CP      = /usr/bin/cp
LD	= $(TOOLROOT)/usr/bin/ld
MKDIR	= /usr/bin/mkdir
NROFF   = $(TOOLROOT)/usr/bin/nroff
RM      = /usr/bin/rm -f
RMR     = $(RM) -r
SHELL	= /bin/sh
ECHO	= echo

#
# compiler definitions
#
PCDEFS   =
PCXXDEFS =

#
# compiler options
#
PCOPTS   = -xansi -fullwarn -n32 -mips3
PCXXOPTS = -fullwarn -n32 -mips3
COPT     = -DNDEBUG -O3
CXXOPT   = -DNDEBUG -O3 -OPT:fold_arith_limit=3000
CDEBUG   = -DDEBUG -g
CXXDEBUG = -DDEBUG -g

# warnings switched off:
#   1174 -- variable declared but never referenced
#   1209 -- controlling expression is constant
#   1233 -- explicit type is missing
#   1314 -- value copied to temporary, reference to temporary used
#   1375 -- destructor is not virtual
#   1376 -- no accessible constructor
#   1681 -- functions don't match -- virtual function override intended?
CWOFF    = -diag_suppress 1174
CXXWOFF  = -diag_suppress 1174,1209,1233,1314,1375,1376,1681

#
# search paths
#
PCINCS   = -I$(ROOT)/usr/include
PCXXINCS = -I$(ROOT)/usr/include

#
# linker options
#
PLDOPTS  = -Wl,-wall -L$(ROOT)/usr/lib32

#
# dirt
#
PDIRT    = *.[eou] a.out core

#
# on irix 6.2 and earlier we have libaudio.a and on later irix versions
# we have libaudio.so.  in AUDIO_LIBS we need to include `-B static'
# before -laudio if we need to link against libaudio.a.  this macro
# decides at link time if it's necessary or not based on whether
# libaudio.so exists.
#
AUDIO_LINKMODE = `if test ! -f $${ROOT}/usr/lib/libaudio.so; then \
		echo '-B static'; fi`

#
# libraries
#
AUDIO_LIBS =				\
	$(AUDIO_LINKMODE)		\
	-laudio				\
	-B dynamic			\
	$(NULL)

DISPLAY_LIBS =				\
	-lXsgivc			\
	$(NULL)

GL_LIBS =				\
	-lGLU				\
	-lGL				\
	$(NULL)

IMAGE_LIBS =				\
	$(NULL)

NETWORK_LIBS =				\
	$(NULL)

WINDOW_LIBS =				\
	-lXext				\
	-lX11				\
	$(NULL)

